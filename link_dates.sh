#!/bin/bash

# Link given time with given arduino time
#
# Pass in a datetime, an arduino time the datetime correlates with, and a csv.
# Out comes all the times of bucket tips and startup time.
#
# Assumes the datetime passed in can be parsed by date -d.
# Assumes the arduino time is milliseconds since arduino startup.
#
# Copyright 2016 Jesse Bickel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

if [[ ! $1 || ! $2 || ! $3 ]]
then
    echo "Usage: ./link_dates.sh 2016-05-03T06:55:18-0500 85800002 rain.csv"
    exit 1
fi

iso="%Y-%m-%dT%H:%M:%S%z"

#example:
#end=$(date -d '2016-05-03T06:55:18-0500' +%s)
end=$(date -d $1 +%s)
secs=$(expr $2 / 1000)
echo "Datetime: $end"
echo "Seconds passed in: $secs"
start=`date -d @$(expr $end - $secs) +$iso`
echo "Start date: $start"
#cat $3 | grep "DATA\|startup" | cut -d',' -f 1
cat $3 | grep "DATA\|startup" | cut -d',' -f 1 | while read time
do
    recsecs=$(expr $time / 1000)
    result=`date -d @$(expr $end - $secs + $recsecs) +$iso`
    echo $result
done


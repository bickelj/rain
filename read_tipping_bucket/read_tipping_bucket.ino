/**
 * Read a reed switch, the one from a rain gage.
 * Each time the switch is open then closed, send
 * a message over serial port.
 * Also send a heartbeat message to tell the consumer we are alive.
 * 
 * Copyright 2016 Jesse Bickel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const int LED = 13;
const long HEARTBEAT = 120000;
long last_beat = 0;
bool on = false;

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print(millis());
  Serial.println(",\"INFO\",\"startup\"");
}

void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  if (sensorValue >= 512 && !on ) {
    on = true;
    digitalWrite(LED, HIGH);
    Serial.print(millis());
    Serial.println(",\"DATA\",\"tipped\"");
  }
  else if (sensorValue < 512 && on) {
    on = false;
    digitalWrite(LED, LOW);
  }
  heartbeat();
  delay(10);
}

void heartbeat(){
  if (millis() / HEARTBEAT > last_beat) {
    Serial.print(millis());
    Serial.println(",\"INFO\",\"heartbeat\"");
    last_beat++;
  }
}
